Screenshots
========

1. *shot01* - Output in Mac console; Part 1
2. *shot02* - Output in Mac console; Part 2
3. *shot03* - View with Produce and Deploy Jobs for DEV, STAGE and PROD in Jenkins; Part 1
4. *shot04* - View with Produce and Deploy Jobs for DEV, STAGE and PROD in Jenkins; Part 2
5. *shot05* - View with Produce and Deploy Jobs for DEV, STAGE and PROD in Jenkins; Part 3
6. *shot06* - View with Produce and Deploy Jobs for DEV, STAGE and PROD in Jenkins; Part 4
7. *shot07* - Output in Jenkins console for a feature test; Part 1
8. *shot08* - Output in Jenkins console for a feature test; Part 2
9. *shot09* - Output in Jenkins console for a feature test; Part 3
10. *shot10* - Running tests on sikuli agents on windows VMs
11. *shot11* - Job View with builds, test results and other informations
12. *shot12* - Successful test report
13. *shot13* - Unsucessful test report
14. *shot14* - Details on the failed test
15. 

