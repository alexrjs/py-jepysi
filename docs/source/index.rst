.. JePySi documentation master file, created by
   sphinx-quickstart on Fri Feb  7 20:15:04 2014.


Welcome to JePySi
=================

Contents
~~~~~~~~~
.. toctree::
   :maxdepth: 2

   Intro
   Tutorial

Modules
~~~~~~~~
.. toctree::
   :maxdepth: 2

   addons <Module/addons>
   base <Module/base>
   case <Module/case>
   cdata <Module/cdata>
   context <Module/context>
   counts <Module/counts>
   image <Module/image>
   region <Module/region>
   runner <Module/runner>

Indices and tables
~~~~~~~~~~~~~~~~~~

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

